package cscie55.project;

import static org.junit.Assert.assertEquals;

import java.rmi.RemoteException;

import org.junit.Test;

public class AccountTest {
	
	/**
	 * Make sure some accounts can be created without error
	 * @throws RemoteException 
	 */
	@SuppressWarnings("unused")
	@Test
	public void testAccounts() throws RemoteException{
		Account one = new AccountImpl(new AccountInfo(1, 1111), 500.0f);
		Account two = new AccountImpl(new AccountInfo(2, 1111), 0.00f);
	}
	
	/**
	 * Make sure constructor won't take invalid floats
	 */
	
	@Test(expected=RemoteException.class)
	public void testNaNConstruction() throws RemoteException{
		@SuppressWarnings("unused")
		Account A = new AccountImpl(new AccountInfo(1, 1111), Float.NaN);
	}
	@Test(expected=RemoteException.class)
	public void testInfinityBalance() throws RemoteException{
		@SuppressWarnings("unused")
		Account A = new AccountImpl(new AccountInfo(1, 1111), Float.POSITIVE_INFINITY);
	}
	
	@Test(expected=RemoteException.class)
	public void testNegInfinityBalance() throws RemoteException{
		@SuppressWarnings("unused")
		Account A = new AccountImpl(new AccountInfo(1, 1111), Float.NEGATIVE_INFINITY);
	}
	
	/** test getBalance  
	 * @throws RemoteException */
	@Test
	public void getBalanceTest() throws RemoteException {
		Account A = new AccountImpl(new AccountInfo(1, 1111), 0.00f);
		Account B = new AccountImpl(new AccountInfo(2, 1111), 100.00f);
		Account C = new AccountImpl(new AccountInfo(3, 1111), 500.00f);
		assertEquals(0.0f, A.getBalance(), 0.001f);
		assertEquals(100.0f, B.getBalance(), 0.001f);
		assertEquals(500.0f, C.getBalance(), 0.001f);
	}
	
	 /** test withdraw
	  */
	 @Test
	 public void withdrawTest() throws RemoteException {
		 Account A = new AccountImpl(new AccountInfo(1, 1111), 2000.00f);
		 float origBalance = A.getBalance();
		 A.withdraw(1000.0f);
		 float newBalance = A.getBalance();
		 assertEquals(1000.0f, origBalance - newBalance, .000001);
	 }
	 
	 /** test overdraw
	  */
	 @Test(expected=InsufficientFundsException.class)
	 public void overdrawTest() throws RemoteException {
		 Account A = new AccountImpl(new AccountInfo(1, 1111), 0.00f);
		 A.withdraw(1000.0f);
	 }
	 
	/** test deposit
	 */
	 @Test
	 public void depositTest() throws RemoteException {
		 Account A = new AccountImpl(new AccountInfo(1, 1111), 0.00f);
		 float origBalance = A.getBalance();
		 A.deposit(1000.0f);
		 float newBalance = A.getBalance();
		 assertEquals(1000.0f, newBalance - origBalance, .000001);
	 }
}