/**
 * Bank creates and keeps track of Account objects, and gives remote references with <code>getAccount</code>
 * 
 * Bank should be made available in the registry under //<hostname>/Bank
 * @see BankServer
 */

package cscie55.project;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Bank extends Remote {
	public void createSampleAccounts() throws RemoteException;
	public Account getAccount(int id) throws RemoteException;
}