/**
 * Authentication mechanism checks pin. Authorization mechanism checks permissions. Permissions and pin are associated with Account according
 * to id. Security should be RMI resgistered under //<hostname>/Security
 * 
 * @see BankServer
 */

package cscie55.project;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

public class SecurityImpl extends UnicastRemoteObject implements Security {

	/**
	 * generated by Eclipse
	 */
	private static final long serialVersionUID = 1153675946976817142L;
	
	/** Map for retrieving Permissions objects by Account id */
	Map<Integer, Permissions> perms;
	
	/** Map for retrieving pins */
	Map<Integer, Integer> pins;

	protected SecurityImpl() throws RemoteException {
		super();	// this exports the remote object by calling UnicastRemoteObject()
		perms = new HashMap<Integer, Permissions>();
		pins = new HashMap<Integer, Integer>();
	}

	@Override
	public boolean isAuthenticated(AccountInfo ai) throws RemoteException {
		int id = ai.getId();
		int attemptPin = ai.getPin();
		int correctPin = pins.get(id);
		return (attemptPin == correctPin);
	}

	@Override
	public boolean isAuthorized(TransactionNotification tn, AccountInfo ai) throws RemoteException {
		Permissions p = perms.get(ai.getId());
		if (tn.getTransType().equals("withdraw")) {
			return p.getWithdrawPerm();
		} else if (tn.getTransType().equals("deposit")) {
			return p.getDepositPerm();
		} else if (tn.getTransType().equals("balance")) {
			return p.getBalancePerm();
		} else if (tn.getTransType().equals("transfer")) {
			int targetId = ((TransferNotification) tn).getTargetId();
			Permissions pTarget = perms.get(targetId);
			return ( p.getWithdrawPerm() && pTarget.getDepositPerm() );
		} else {
			throw new SecurityException("Bad transaction string");
		}
	}

	@Override
	public void setPermissions(AccountInfo ai, Permissions p) throws RemoteException{
		perms.put(ai.getId(), p);
		pins.put(ai.getId(), ai.getPin());
	}
}