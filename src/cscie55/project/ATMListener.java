/**
 * this is an interface implemented by Client and any other listener who wants to subscribe to Transaction Notifications
 * from an atm. 
 */

package cscie55.project;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ATMListener extends Remote {
	public void handleNotification(TransactionNotification tn) throws RemoteException;
}