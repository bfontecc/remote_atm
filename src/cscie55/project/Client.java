/**
 * Our atm Client tests the atm's functionality. It is the Client's job to:
 * 
 * Look up the ATMFactory in the registry
 * Register Itself with the ATM as an ATMListener
 * (in this case) Run battery of tests against atm
 * 
 * @author Bret Fontecchio
 * @author Charlie Sawyer
 */

package cscie55.project;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.UnknownHostException;
import java.rmi.server.UnicastRemoteObject;

public class Client extends UnicastRemoteObject implements ATMListener {
	
	/**
	 * generated by Eclipse
	 */
	private static final long serialVersionUID = 2343146909109736319L;
	
	public Client() throws RemoteException {
		super();
	}
	
	public static void  main(String [] args) {
		ATMListener c = null;
		try {
			c = new Client();
		} catch (RemoteException e1) {
			e1.printStackTrace();
		}
		ATM atm = null;
	      try {
	         ATMFactory factory = (ATMFactory)Naming.lookup("//localhost/atmfactory");
	         atm = factory.getATM();
	      } catch (MalformedURLException mue) {
	         mue.printStackTrace();
	      } catch (NotBoundException nbe) {
	         nbe.printStackTrace();
	      } catch (UnknownHostException uhe) {
	         uhe.printStackTrace();
	      } catch (RemoteException re) {
	         re.printStackTrace();
	      }
	      if (c != null) {
		      if (atm!=null) {
		    	  try {
					atm.registerListener(c);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
		    	// run tests
		        testATM(atm);
		      }
	      }
	}
	
	/**
	 * notification handler makes implicit call to toString method in notification object
	 */
	public void handleNotification(TransactionNotification tn) throws RemoteException {
		System.out.println(tn);
	}
	
	/**
	 * Factory for AccountInfo objects
	 * 
	 * @param id
	 * @param pin
	 * @return AccountInfo
	 */
	public static AccountInfo getAccountInfo(int id, int pin) {
		return new AccountInfo(id, pin);
	}
	
	public static void testATM(ATM atm) {
	      if (atm!=null) {
	         printBalances(atm);
	         performTestOne(atm);
	         performTestTwo(atm);
	         performTestThree(atm);
	         performTestFour(atm);
	         performTestFive(atm);
	         performTestSix(atm);
	         performTestSeven(atm);
	         performTestEight(atm);
	         performTestNine(atm);
	         printBalances(atm);
	      }
	   }        
	   public static void printBalances(ATM atm) {        
	      try {
	         System.out.println("Balance(0000001): "+atm.getBalance(getAccountInfo(0000001, 1234)));
	         System.out.println("Balance(0000002): "+atm.getBalance(getAccountInfo(0000002, 2345)));
	         System.out.println("Balance(0000003): "+atm.getBalance(getAccountInfo(0000003, 3456)));
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	   }
	   public static void performTestOne(ATM atm) {       
	      try {
	         atm.getBalance(getAccountInfo(0000001, 5555));
	      } catch (Exception e) {
	         System.out.println("Failed as expected: "+e);
	      }
	   }
	   public static void performTestTwo(ATM atm) {       
	      try {
	         atm.withdraw(getAccountInfo(0000002, 2345), 500);
	      } catch (Exception e) {
	         System.out.println("Failed as expected: "+e);
	      }
	   }
	   public static void performTestThree(ATM atm) {        
	      try {
	         atm.withdraw(getAccountInfo(0000001, 1234), 50);
	      } catch (Exception e) {
	         System.out.println("Failed as expected: "+e);
	      }
	   }
	   public static void performTestFour(ATM atm) {         
	      try {
	         atm.deposit(getAccountInfo(0000001, 1234), 500);
	      } catch (Exception e) {
	         System.out.println("Unexpected error: "+e);
	      }
	   }
	   public static void performTestFive(ATM atm) {         
	      try {
	         atm.deposit(getAccountInfo(0000002, 2345), 100);
	      } catch (Exception e) {
	         System.out.println("Unexpected error: "+e);
	      }
	   }
	   public static void performTestSix(ATM atm) {       
	      try {
	         atm.withdraw(getAccountInfo(0000001, 1234), 100);
	      } catch (Exception e) {
	         System.out.println("Unexpected error: "+e);
	      }
	   }
	   public static void performTestSeven(ATM atm) {        
	      try {
	         atm.withdraw(getAccountInfo(0000003, 3456), 300);
	      } catch (Exception e) {
	         System.out.println("Unexpected error: "+e);
	      }
	   }
	   public static void performTestEight(ATM atm) {        
	      try {
	         atm.withdraw(getAccountInfo(0000001, 1234), 200);
	      } catch (Exception e) {
	         System.out.println("Failed as expected: "+e);
	      }
	   }
	   public static void performTestNine(ATM atm) {        
	      try {
	         atm.transfer(getAccountInfo(0000001, 1234),getAccountInfo(0000002, 2345), 100);
	      } catch (Exception e) {
	         System.out.println("Unexpected error: "+e);
	      }
	   }
}
