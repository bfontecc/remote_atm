/**
 * ATMServer creates and registers the ATMFactory, which is the entry point to ATM functionality
 */

package cscie55.project;

import java.rmi.Naming;

public class ATMServer {
	public static void main(String [] args) throws ATMException {
		ATMFactory factory;
		try {
			factory = new ATMFactoryImpl();
			System.out.println("Created new factory object.");
			Naming.rebind("//localhost/atmfactory", factory);
			System.out.println("atmfactory bound in registry");
		} catch (Exception e) {
			System.out.println("ATMServer Exception: " + e.getMessage());
			e.printStackTrace();
		}
	}
}