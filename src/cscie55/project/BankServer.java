/**
 * The BankServer must:
 * Create Bank object and rebind it in registry. 
 * Create Security object and rebind it in registry. 
 */

package cscie55.project;

import java.rmi.Naming;

public class BankServer {

	public static void main(String [] args) throws BankException {
		Bank bank;
		Security sec;
		try {
			sec = new SecurityImpl();
			System.out.println("\nCreated new Security object");
			Naming.rebind("//localhost/Security", sec);
			System.out.println("Named Security in registry.");
		} catch (Exception e) {
			System.out.println("BankServer Exception: " + e.getMessage());
			e.printStackTrace();
		}
		try {
			bank = new BankImpl();
			System.out.println("\nCreated new Bank object");
			Naming.rebind("//localhost/Bank", bank);
			System.out.println("Named Bank in registry.");
		} catch (Exception e) {
			System.out.println("BankServer Exception: " + e.getMessage());
			e.printStackTrace();
		}
	}

}