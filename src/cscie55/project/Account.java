/**
 * Account keeps a balance for a certain account id, stores the account pin, and includes functionality for performing
 * modifications to the balance (if funds are sufficient).
 * 
 *  Use of Account:
 * 	preconditions:	The client has authenticated with Security. Bank has chosen correct account. Bank has verified pin.
 * 	postconditions:	Persistent Account object is updated in Bank.
 * 
 * @see AccountImpl
 */

package cscie55.project;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Account extends Remote {
	public void deposit(float amount) throws RemoteException;
	public void withdraw(float amount) throws RemoteException;
	public float getBalance() throws RemoteException;
	public int getId() throws RemoteException;
}