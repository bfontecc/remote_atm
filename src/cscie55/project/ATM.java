package cscie55.project;

import java.rmi.RemoteException;

/**
 * ATM remote interface defines the contract for ATM operations usch as deposit, withdraw, and check balance. As of version 2.0, this is a
 * remote interface, accessible over a network connection.
 * 
 * @author Bret Fontecchio
 * @version 2.0
 * @since January 17, 2013
 * 
 * @see ATMImpl.java
 */

public interface ATM extends java.rmi.Remote {
	public void deposit(AccountInfo ai, float amount) throws RemoteException;
	public void withdraw(AccountInfo ai, float amount) throws RemoteException;
	public float getBalance(AccountInfo ai) throws RemoteException;
	public void transfer(AccountInfo from, AccountInfo to, float amount) throws RemoteException;
	public void registerListener(ATMListener l) throws RemoteException;
}