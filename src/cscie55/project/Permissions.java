/**
 * Object holding boolean values for Account operation
 * Typical accessors, mutators and constructor. 
 */

package cscie55.project;

import java.io.Serializable;

public class Permissions implements Serializable {
	
	/**
	 * generated by Eclipse
	 */
	private static final long serialVersionUID = -8318347328491555279L;
	
	private boolean withdraw;
	private boolean deposit;
	private boolean balance;
	
	public Permissions(boolean withdraw, boolean deposit, boolean balance) {
		this.withdraw = withdraw;
		this.deposit = deposit;
		this.balance = balance;
	}
	
	public boolean getWithdrawPerm() {
		return withdraw;
	}
	public boolean getDepositPerm() {
		return deposit;
	}
	public boolean getBalancePerm() {
		return balance;
	}
	
	protected void setWithdrawPerm(boolean withdraw) {
		this.withdraw = withdraw;
	}
	protected void setDepositPerm(boolean deposit) {
		this.deposit = deposit;
	}
	protected void setBalancePerm(boolean balance) {
		this.balance = balance;
	}
}