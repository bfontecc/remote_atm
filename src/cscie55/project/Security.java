/**
 * Security Keeps track of permissions and pins for account objects
 */

package cscie55.project;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Security extends Remote {
	public boolean isAuthenticated(AccountInfo ai) throws RemoteException;
	public boolean isAuthorized(TransactionNotification tn, AccountInfo ai) throws RemoteException;
	public void setPermissions(AccountInfo ai, Permissions p) throws RemoteException;
}